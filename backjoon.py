# -*- coding: utf-8 -*-
import json
import re
import urllib.request
import urllib.parse

from bs4 import BeautifulSoup
from flask import Flask, request
from slack import WebClient
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slack.web.classes.interactions import MessageInteractiveEvent
from slackeventsapi import SlackEventAdapter
from threading import Thread

import random

SLACK_TOKEN = ?
SLACK_SIGNING_SECRET = ?

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


def printAlgorithmList(url):
    hdr = {'User-Agent': 'Mozilla/5.0', 'referer': 'https://www.acmicpc.net'}
    req = urllib.request.Request(url, headers=hdr)
    source_code = urllib.request.urlopen(req).read()
    soup = BeautifulSoup(source_code, "html.parser")

    algorithmNameList = []
    algorithmLinkList = []
    for i, tag in enumerate(soup.find_all("td")):
        if len(algorithmNameList) >= 10:
            break

        if i % 2 == 0:
            algorithmNameList.append(tag.get_text().strip())

        ## find algorithm link ##
        a_tag = tag.find("a")
        if a_tag != None and 'problem/' in a_tag["href"]:
            algorithmLinkList.append('https://www.acmicpc.net' + a_tag.get("href"))
        ## END: find algorithm link ##

    return algorithmLinkList, algorithmNameList


def printQuestion(url, b_rand=False):
    hdr = {'User-Agent': 'Mozilla/5.0', 'referer': 'https://www.acmicpc.net'}
    req = urllib.request.Request(url, headers=hdr)
    source_code = urllib.request.urlopen(req).read()
    soup = BeautifulSoup(source_code, "html.parser")

    if not b_rand:
        questionLinks = []
        for td_tag in soup.find_all("td"):
            a_tag = td_tag.find("a")
            if a_tag != None and 'problem/' in a_tag["href"]:
                questionLinks.append('https://www.acmicpc.net' + a_tag.get("href"))

        ## access question page ##
        selected_Q = questionLinks[random.randrange(0, len(questionLinks)-1)]

    else:
        selected_Q = url

    req = urllib.request.Request(selected_Q, headers=hdr)
    source_code = urllib.request.urlopen(req).read()
    soup = BeautifulSoup(source_code, "html.parser")

    question_name = soup.find("span", id="problem_title")
    question_tag = soup.find("section", id="description")
    name = "*"+question_name.get_text().strip() + '*   '
    question = question_tag.get_text().strip()

    question_section = []
    question_section.append(SectionBlock(text=''.join(name + '\n<'+selected_Q+"|문제 바로가기>\n\n\n\n" + question)))

    ## loads image ##
    question_img = soup.find_all("img", alt="")
    if question_img != None:
        for q_img in question_img:
            img_src = q_img.get("src")
            img_url = img_src
            img = ImageBlock(image_url=img_url, alt_text="None")
            question_section.append(img)
    ## END: loads image ##


    ## END: access question page ##

    return question_section

def createStartButtonSection():
    section = []

    section.append(SectionBlock(text="원하는 문제 종류를 선택해주세요.\n"))
    section.append(
        ActionsBlock(block_id="start_button",
                     elements=[
                         ButtonElement(text="알고리즘별 문제",
                                       action_id="button_algorithm",
                                       value="1"),
                         ButtonElement(text="삼성 SW 테스트 문제",
                                       action_id="button_samsung",
                                       value="2"),
                         ButtonElement(text="랜덤 문제",
                                       action_id="button_random",
                                       value="3"),
                     ])
    )

    return section


def createAlgorithmButtonSection(url):
    algo_linkList, algo_nameList = printAlgorithmList(url)
    section = []

    section.append(SectionBlock(text="원하는 문제 종류를 선택해주세요.\n"))
    section.append(
        ActionsBlock(block_id="start_button",
                    elements=[
                        ButtonElement(text=algo_nameList[0],
                                      action_id="button_dynamic",
                                      value="4"),
                        ButtonElement(text=algo_nameList[1],
                                      action_id="button_math",
                                      value="5"),
                        ButtonElement(text=algo_nameList[2],
                                      action_id="button_implement",
                                      value="6"),
                        ButtonElement(text=algo_nameList[3],
                                      action_id="button_graph",
                                      value="7"),
                        ButtonElement(text=algo_nameList[4],
                                      action_id="button_greedy",
                                      value="8")
                    ])
    )
    return section, algo_linkList

links = []
def tt(action_id, channel):
    global links

    if action_id == "button_algorithm":
        url = "https://www.acmicpc.net/problem/tags"
        # section = printAlgorithmList(url, channel)
        section, links = createAlgorithmButtonSection(url)

    elif action_id == "button_samsung":
        url = "https://www.acmicpc.net/workbook/view/1152"
        section = printQuestion(url)

    elif action_id == "button_random":
        url = "https://www.acmicpc.net/problem/random"
        b_rand = True
        section = printQuestion(url, b_rand)

    elif action_id == "button_dynamic":
        url = links[0]
        section = printQuestion(url)

    elif action_id == "button_math":
        url = links[1]
        section = printQuestion(url)

    elif action_id == "button_implement":
        url = links[2]
        section = printQuestion(url)

    elif action_id == "button_graph":
        url = links[3]
        section = printQuestion(url)

    elif action_id == "button_greedy":
        url = links[4]
        section = printQuestion(url)

    slack_web_client.chat_postMessage(
        channel=channel,
        blocks=extract_json(section)
    )



# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    if "시작" in text:
        section = createStartButtonSection()

    elif "끝" in text:
        section = [(SectionBlock(text="문제 풀이가 끝났습니다. 고생하셨습니다!\n"))]

    elif ("다른 문제" in text) or ("다른문제" in text):
        section = createStartButtonSection()

    slack_web_client.chat_postMessage(
        channel=channel,
        blocks=extract_json(section)
    )


@app.route("/click", methods=["GET", "POST"])
def on_button_click():

    payload = request.values["payload"]
    click_event = MessageInteractiveEvent(json.loads(payload))

    action_id = click_event.action_id
    channel = click_event.channel.id

    Thread(target=tt, args=(action_id, channel)).start()


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=8080)